<?php

return [

    // Administrator's email
    'email' => 'tim.tawan@signaturegroup.com.au',
    'emails' => [
        'tim.tawan@signaturegroup.com.au',
        'luke.peters@sportingglobe.com.au',
        'marketing@sportingglobe.com.au',
        'jess.obrienchurch@signaturegroup.com.au',
        'matthew@radiusmedia.com.au',
    ],

    // Number of birth day range
    'dayRage' => 15,
];
