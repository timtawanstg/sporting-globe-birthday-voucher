<?php

use Illuminate\Database\Seeder;

class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Tim Tawan',
            'role_id' => 1,
            'email' => 'tim.tawan@signaturegroup.com.au',
            'password' => Hash::make('password'),
        ]);
    }
}
