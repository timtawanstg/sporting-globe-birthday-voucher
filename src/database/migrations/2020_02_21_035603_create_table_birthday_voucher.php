<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBirthdayVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('birthday_voucher', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('account_id');
            $table->string('voucherId');
            $table->string('lookupId');
            $table->string('voucher_created_at');
            $table->string('voucher_expiry_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('birthday_voucher');
    }
}
