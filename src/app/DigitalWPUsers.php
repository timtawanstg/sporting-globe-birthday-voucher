<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DigitalWPUserMeta;

class DigitalWPUsers extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'digital';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wp_users';

    /**
     * Undocumented function
     *
     * @return void
     */
    public function meta() {
        return $this->hasMany(DigitalWPUserMeta::class, 'user_id', 'ID');
    }

}
