<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BirthdayUsers extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'birthday_users';
}
