<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\VoucherAccountCollection;
use App\Repositories\AccountVoucherRepository;

use Illuminate\Http\Request;
use App\Http\Requests\CreateAccountVoucherRequest;
use Carbon\Carbon;
use Exception;
class VoucherAccountController extends Controller
{
    /**
     * The account voucher repository implementation
     *
     * @var App\Repositories\AccountVoucherRepository
     */
    public $accountVoucher;

    /**
     * Create a new controller instance.
     *
     * @param  App\Repositories\AccountVoucherRepository  $accountVoucher
     * @return void
     */
    public function __construct(AccountVoucherRepository $accountVoucher) {
        $this->accountVoucher = $accountVoucher;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAccountVoucherRequest $request)
    {
        try {
            $user_id = $request->input('user_id');
            $account = $this->accountVoucher->create($user_id);

            return response([
                'data' => $account,
            ], 201);

        } catch (Exception $e) {
            return response([
                'errorMessage' => $e->getMessage(),
            ], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $account_id
     * @return \Illuminate\Http\Response
     */
    public function show($account_id)
    {
        try {
            $account_voucher = $this->accountVoucher->findById($account_id);
            $voucher_created_at = Carbon::create($account_voucher->get('created_at'));

            if ($voucher_created_at->diffInDays(now()) > 30) {
                throw new Exception('Voucher is expired');
            }

            return new VoucherAccountCollection($account_voucher);
        } catch (Exception $e) {
            return response([
                'data' => null,
                'errorMessage' => $e->getMessage(),
            ], 403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
