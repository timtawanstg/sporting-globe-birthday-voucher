<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VoucherAccountCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item = $this->collection;

        if (empty($item)) {
            return [];
        }

        return [
            'voucherId' => $item->get('voucherId'),
            'lookupId' => $item->get('lookupId'),
            'voucher_created_at' => $item->get('voucher_created_at'),
            'voucher_expiry_at' => $item->get('voucher_expiry_at'),
            'account_id' => $item->get('account_id'),
            'updated_at' => $item->get('updated_at'),
            'created_at' => $item->get('created_at'),
            'id' => $item->get('id'),
        ];
    }
}
