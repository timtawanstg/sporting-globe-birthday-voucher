<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;
use App\User;

class HorizonAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            throw_if(
                !Auth::guard($guard)
                ->user()
                ->isAdmin(),
                AuthorizationException::class,
                'You are not allowed to access this page'
            );

            return $next($request);
        }

        throw_if(
            is_null($request->get('key')),
            AuthorizationException::class,
            'You are not allowed to access this page without authorization'
        );

        try {
            $user = User::where('password', $request->get('key'))->first();

            throw_if(
                is_null($user) && !$user->isAdmin(),
                AuthorizationException::class,
                'Unauthorized'
            );

            // Login
            Auth::login($user);

            return $next($request);
        } catch (AuthorizationException $er) {
            abort($er);
        }
    }
}
