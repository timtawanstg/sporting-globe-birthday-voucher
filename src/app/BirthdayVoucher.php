<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BirthdayVoucher extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'birthday_voucher';
}
