<?php

namespace App\Contracts;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;

interface AccountVoucherRepository {
    /**
     * Retrieve the account with the given IDs.
     *
     * @param integer $account_id
     * @return \Illuminate\Support\Collection
     */
    public function findById(int $account_id);

    /**
     * Create voucher
     *
     * @param string $user_id
     * @return boolean
     */
    public function create(string $user_id);
}
