<?php

namespace App\Traits;
use App\Roles;

/**
 * Class Roles
 *
 * @method static Builder|Collection|\Eloquent
 */

trait UseRoles {

    /**
     * To find user's role
     *
     * @param string $slugName slug role
     * @return boolean
     */
    public function hasPermission($slugName) {
        $user_role_id = $this->role_id;

        $roleModel = new Roles;

        $user_role = $roleModel
        ->where('slug', $slugName)
        ->where('id', $user_role_id)
        ->first();

        if (is_null($user_role)) {
            return false;
        }

        return true;
    }
}
