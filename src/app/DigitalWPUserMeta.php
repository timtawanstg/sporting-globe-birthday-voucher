<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AllNewUsersToday;

class DigitalWPUserMeta extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'digital';

    /**
     * @var string
     */
    protected $table = 'wp_usermeta';

    /**
     * @var string
     */
    protected $primaryKey = 'umeta_id';

    /**
     * @var array
     */
    protected $fillable = ['meta_key', 'meta_value', 'user_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(AllNewUsersToday::class);
    }
}
