<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllUsersWithDOB extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'digital';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_with_dob';
}
