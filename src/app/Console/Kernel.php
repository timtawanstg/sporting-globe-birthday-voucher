<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\LoadUsers::class,
        Commands\GenerateBirthdayUsers::class,
        Commands\LoadCachedUsers::class,
        Commands\ClearAllAccounts::class,
        Commands\ClearVoucherAdmin::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Horizon metrics
        $schedule->command('horizon:snapshot')
            ->everyThirtyMinutes()
            ->withoutOverlapping()
            ->onOneServer();

        // Production mode
        if (App::environment('production')) {

        }

        // Only for testing
        if (App::environment('staging')) {
            $schedule
                ->command('bv:load_users_from_db')
                ->hourly()
                ->after(function() {
                    $this->call('bv:store_users_in_cache');
                })
                ->emailOutputTo(config('admin.email'))
                ->runInBackground()
                ->withoutOverlapping()
                ->onOneServer();

            $schedule
                ->command('bv:start_birthday_voucher_flow')
                ->everyTenMinutes()
                ->emailOutputTo(config('admin.email'))
                ->withoutOverlapping()
                ->onOneServer();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return 'Australia/Melbourne';
    }
}
