<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

use App\Jobs\LoadUsersJob;
use App\AllUsersWithDOB;
use App\DigitalWPUsers;
use App;
class LoadUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bv:load_users_from_db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Clear existing cache
        // Cache::forget('all_accounts');
        Cache::tags('account')->flush();
        Cache::tags('chunk_accounts')->flush();

        // Print log
        Log::info('Cleared all account info');

        // Load all users
        $allAccounts = AllUsersWithDOB::orderBy('account_id')->get();
        // Save to cache
        foreach($allAccounts->chunk(1000) as $accounts) {
            Cache::tags('chunk_accounts')->put(uniqid(), $accounts->toJson());
        }
        // Print log
        Log::info("Loaded total users: {$allAccounts->count()}");
    }
}
