<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;

use App\Jobs\CreateBirthdayUserJob;

class GenerateBirthdayUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bv:start_birthday_voucher_flow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cachedUsers = $this->loadAllUsersFromCache();

        foreach ($cachedUsers as $user) {
            // Start creating flow
            $value = Cache::tags('voucher')->get($user->get('account_id'));
            if (!is_null($value) || !empty($value)) continue;
            dispatch(new CreateBirthdayUserJob($user));
        }

        Log::info('Created birthday workflow users total => '. count($cachedUsers));
    }

    /**
     * Load all users from cache database
     *
     * @return array all cached users
     */
    private function loadAllUsersFromCache() {
        $redis = Redis::connection('cache');

        $birthday_users = collect();

        // Read all keys in cache
        foreach($redis->keys("*") as $accountKey) {
            if (is_null($accountKey)) continue;

            $detail = explode(":", $accountKey);

            if (count($detail) !== 3) continue;
            if (is_null($detail[2])) continue;

            $account_id = $detail[2];
            $account = Cache::tags('account')->get($account_id);
            // $value = Cache::tags('voucher')->get($user->account_id);

            if (is_null($account)) continue;

            $objectValue = json_decode($account, true);
            $birthday_users->push(collect($objectValue));
        }

        return $birthday_users->unique('account_id');
    }

}
