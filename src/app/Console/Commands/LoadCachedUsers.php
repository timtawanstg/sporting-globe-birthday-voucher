<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

use App\Jobs\LoadUsersJob;
use App\AllUsersWithDOB;
use App;

class LoadCachedUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bv:store_users_in_cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Destruct mess cached users to single cache item';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allAccounts = $this->loadChunkAccounts();

        Log::info('Found cached account ' . count($allAccounts));

        foreach($allAccounts->chunk(100) as $accounts) {
            foreach ($accounts as $account) {

                // Check existing account and issued voucher
                $existingAccountCached = Cache::tags('account')->get($account->get('account_id'));
                $existingVoucherCached = Cache::tags('voucher')->get($account->get('account_id'));

                if (
                    !is_null($existingAccountCached) &&
                    !is_null($existingVoucherCached)
                ) continue;

                // If it is running in staging or local
                // Limit only to admin group
                if (App::environment(['local', 'staging'])) {
                    if ( !in_array($account->get('user_email'), config('admin.emails')) ) {
                        continue;
                    }
                }

                // Save user and cache
                dispatch(new LoadUsersJob($account));
            }
        }

        Log::info('Done cached single account');

    }


    /**
     * Load all chunk account from cache database
     *
     * @return array
     */
    public function loadChunkAccounts() {
        $redis = Redis::connection('cache');

        $chunk_collection_ids = collect();
        $account_collection = collect();

        // Read all keys in cache
        foreach($redis->keys("*") as $accountKey) {
            if (is_null($accountKey)) continue;

            $detail = explode(":", $accountKey);

            if (count($detail) !== 3) continue;
            if (is_null($detail[2])) continue;

            $chunk_account_id = $detail[2];
            $chunk_accounts_json = Cache::tags('chunk_accounts')->get($chunk_account_id);
            if (is_null($chunk_accounts_json)) continue;

            $chunk_collection_ids->push($chunk_account_id);
        }

        Log::info('Found chunk cached ' . count($chunk_collection_ids));

        // Get user info from all chunks
        foreach($chunk_collection_ids as $chunk_id) {
            $chunk_accounts_json = Cache::tags('chunk_accounts')->get($chunk_id);
            $chunk_accounts = json_decode($chunk_accounts_json, true);

            if (is_null($chunk_accounts)) continue;

            foreach ($chunk_accounts as $account) {
                $user = collect($account);
                if (!$user->has('user_id')) continue;

                $account_collection->push($user);
            }
        }

        Log::info('Total users in chunk cached ' . count($account_collection));

        return $account_collection;
    }
}
