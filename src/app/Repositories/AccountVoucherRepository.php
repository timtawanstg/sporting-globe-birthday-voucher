<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Contracts\AccountVoucherRepository as AccountVoucherRepositoryInterface;

use App\Jobs\CreateCacheSingleUserJob;

use Exception;
class AccountVoucherRepository implements AccountVoucherRepositoryInterface
{
    /**
     * The Redis factory implementation.
     */
    public $cache;

    /**
     * Create a new event handler instance.
     *
     * @return void
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Get account voucher by account id
     *
     * @param integer $account_id
     * @return Illuminate\Support\Collection
     */
    public function findById(int $account_id) {
        $valueJson = $this->cache::tags(['voucher'])->get($account_id);
        if (empty($valueJson)) {
            throw new Exception('Data not found');
        }

        return collect(json_decode($valueJson, true));
    }

    /**
     * Create new account to redis
     *
     * @param string $user_id
     * @return void
     */
    public function create(string $user_id) {
        if (is_null($user_id)) throw new Exception('user_id is empty');
        dispatch(new CreateCacheSingleUserJob($user_id));
        return $user_id;
    }
}
