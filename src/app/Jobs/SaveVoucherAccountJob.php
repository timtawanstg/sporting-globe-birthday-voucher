<?php

namespace App\Jobs;

use Exception;
use GuzzleHttp;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use App\BirthdayVoucher;

class SaveVoucherAccountJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     *
     * @return void
     */
    private $user = null;

    /**
     *
     * @return void
     */
    private $apiKey = null;

    /**

     * @return void
     */
    private $endpointUrl = null;

    /**
     * voucherSetupId
     *
     * @var void
     */
    private $voucherSetupId = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->endpointUrl = env('POS_API_ENDPOINT', 'https://apis-sandbox.sportingglobe.com.au/pos/voucher');
        $this->apiKey = env('SGH_API_KEY', 'zyao9QAvKk6mUOL0uSqOa7zVbI9Q6qAx1J7MLs5t');
        $this->voucherSetupId = env('VOUCHER_SETUP_ID', 448);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;

        $existing_user = Cache::tags(['voucher'])
        ->get($user->account_id);

        $existing_voucher = BirthdayVoucher::where('account_id', $user->account_id)
        ->exists();

        if (
            $existing_voucher ||
            !is_null($existing_user)
        ) {
            return false;
        }

        $param = [
            'accountId' => $user->account_id,
            'email' => $user->email,
            'voucherSetupId' =>  $this->voucherSetupId
        ];

        $headers = array(
            'x-api-key' => $this->apiKey,
        );

        $client = new GuzzleHttp\Client([
            'debug' => false
        ]);

        $request = $client->request('POST', $this->endpointUrl, [
            'form_params' => $param,
            'headers' => $headers,
        ]);

        if ($request->getStatusCode() !== 201) {
            throw new Exception('Can not create voucher');
        }

        $response = $request->getBody();
        $responseJson = json_decode($response, true);
        $voucherItem = $responseJson['data'];

        $voucher = new BirthdayVoucher;

        $voucher->voucherId = $voucherItem['id'];
        $voucher->lookupId = $voucherItem['lookupId'];
        $voucher->voucher_created_at = $voucherItem['createdAt'];
        $voucher->voucher_expiry_at = $voucherItem['dateExpiry'];
        $voucher->account_id = $user->account_id;
        $voucher->save();

        Cache::tags('voucher')
            ->put(
                $user->account_id,
                $voucher->toJson(),
                now()->addYear()
            );
    }
}
