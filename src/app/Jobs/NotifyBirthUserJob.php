<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GuzzleHttp\Exception as GuzzleHttpException;

use App;
use Exception;
use GuzzleHttp;
use App\WorkflowInfo;
use App\BirthdayUsers;

class NotifyBirthUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     *
     * @return void
     */
    private $user = null;

    /**
     *
     * @return void
     */
    private $apiKey = null;

    /**
     * @return void
     */
    private $endpointUrl = null;

    /**

     * @return void
     */
    private $workflowId = null;

    /**

     * @return void
     */
    private $workflowEmailId = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->apiKey = env('MAILCHIMP_API_KEY', '08810c95f386ddbf72c9b13eac9367a9-us5');
        $this->loadWorkflow();
    }

    private function loadWorkflow() {

        $workflow = WorkflowInfo::first();
        $mailServerUrl = env('MAILCHIMP_API_ENDPOINT', 'https://us5.api.mailchimp.com/3.0');

        $this->workflowId = $workflow->workflow_id;
        $this->workflowEmailId = $workflow->workflow_email_id;
        $this->endpointUrl = "{$mailServerUrl}/automations/{$this->workflowId}/emails/{$this->workflowEmailId}/queue";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;

        $user_email = $user->email;

        $param = [
            'email_address' => $user_email,
        ];

        $auth = ['any', $this->apiKey];

        $client = new GuzzleHttp\Client([
            'debug' => false
        ]);

        try {
            $client->request('POST', $this->endpointUrl, [
                'headers' => [
                    'Content-type' => 'application/json',
                ],
                'auth' => $auth,
                'json' => $param,
            ]);
        } catch (GuzzleHttpException $error) {
            throw new Exception($error);
        }
    }
}
