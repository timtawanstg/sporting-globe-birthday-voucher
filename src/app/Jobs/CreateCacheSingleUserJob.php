<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

use App\Jobs\BirthdayVoucherJob;
use App\Jobs\LoadUsersJob;
use App\Jobs\CreateBirthdayUserJob;
use App\BirthdayVoucher;
use App\DigitalWPUsers;

use stdClass;
use Exception;

class CreateCacheSingleUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * User info
     *
     * @var
     */
    private $user_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->getUser();
        // start
        BirthdayVoucherJob::withChain([
            new LoadUsersJob($user),
            new CreateBirthdayUserJob($user),
        ])->dispatch();
    }

    public function getUser() {
        if (is_null($this->user_id)) throw new Exception('user_id is empty');
        // Get user info
        $user = DigitalWPUsers::where('ID', $this->user_id);
        // Find user
        if (!$user->first()) {
            throw new Exception("{$this->user_id} is not found");
        }

        $allowKeys = [
            'account_id',
            'verification_token_used',
            'date_day',
            'date_month',
            'date_year',
            'birthday',
        ];

        $newUser = new stdClass;

        foreach($user->first()->meta as $meta) {
            if (in_array($meta->meta_key, $allowKeys)) {
                $newUser->{$meta->meta_key} = $meta->meta_value;
                $newUser->date_month = $meta->meta_key === 'date_month' ? $meta->meta_value : null;
                $newUser->date_year = $meta->meta_key === 'date_year' ? $meta->meta_value : null;
                $newUser->date_day = $meta->meta_key === 'date_day' ? $meta->meta_value : null;
            }
        }

        $newUser->ID = $user->ID;
        $newUser->user_id = $user->ID;
        $newUser->user_email = $user->user_email;
        $newUser->user_registered = $user->user_registered;

        // Create collection type
        $user = collect($newUser);

        // Check if this user already created a voucher
        // $birthdayUserExistence = BirthdayVoucher::where('account_id', $user->get('account_id'));

        // // Throw exist
        // if ($birthdayUserExistence->exists()) throw new Exception("{$user->get('account_id')} is already issued a voucher");

        return $user;
    }

}
