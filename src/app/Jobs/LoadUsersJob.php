<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class LoadUsersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user = null;
    private $account_id = null;
    private $detail = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->account_id = $user->get('account_id');
        $this->detail = $user->toJson();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Cache::tags('account')
            ->put(
                $this->account_id,
                $this->detail,
                now()->addDay(15)
            );
    }
}
