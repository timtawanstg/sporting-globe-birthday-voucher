<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

use App\Jobs\SaveVoucherAccountJob;
// use App\Jobs\SaveBirthdayUserJob;
use App\Jobs\BirthdayVoucherJob;
use App\Jobs\NotifyBirthUserJob;

use Carbon\Carbon;
use stdClass;
use Exception;

class CreateBirthdayUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * User info
     *
     * @var
     */
    private $user = null;

    /**
     * Number of days prior user's birthday
     *
     * @var
     */
    private $rangeDay = null;

    /**
     * Current date/time
     *
     * @var
     */
    private $now = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->now = Carbon::now();
        $this->rangeDay = config('admin.dayRage');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $account_id = $this->user->get('account_id');
        $user_email = $this->user->get('user_email');
        $birthdayDate = $this->getBirthdayDate();

        // Skip invalid DOB
        if (!$birthdayDate) return false;
        // Skip existing user
        if ($this->exists()) return false;
        // Check only user in specific day range if not skip
        if ($this->outOfRangeUser($birthdayDate)) return false;

        $birthdayUser = new stdClass();
        $birthdayUser->account_id = $account_id;
        $birthdayUser->email = $user_email;
        $birthdayUser->dob = $birthdayDate->toDateString();

        // Start creating flow
        BirthdayVoucherJob::withChain([
            // Save birthday user db
            // new SaveBirthdayUserJob($birthdayUser),
            // Create voucher and save in db and cache
            new SaveVoucherAccountJob($birthdayUser),
            // Notify user via mailchimp
            new NotifyBirthUserJob($birthdayUser),
        ])->dispatch();
    }

    /**
     * Check user existing
     *
     * @param string $account_id
     * @return boolean
     */
    private function exists() {
        $value = Cache::tags('voucher')->get($this->user->get('account_id'));
        return !is_null($value) || !empty($value);
    }

    /**
     * Check only user in specific day range
     *
     * @param Carbon $user_birthday
     * @return boolean
     */
    private function outOfRangeUser($birthdayDate) {
        // Find different days
        $birthdayDiffDays = $this->now->diffInDays($birthdayDate, false);
        return $birthdayDiffDays != $this->rangeDay;
    }

    /**
     * Get proper date
     *
     * @return Carbon Carbon datetime instance
     */
    private function getBirthdayDate() {
        try {
            if (
                $this->user->has('birthday') &&
                !empty($this->user->get('birthday'))
            ) {
                list($day, $month, $year) = explode("/", $this->user->get('birthday'));

                if (is_null($day) || is_null($month)) {
                    return false;
                }

                return Carbon::create(
                    $this->now->year,
                    $month,
                    $day
                );
            }

            $date_day = $this->user->get('date_day');
            $date_month = $this->user->get('date_month');

            return Carbon::create(
                $this->now->year,
                $date_month,
                $date_day
            );
        } catch (Exception $e) {
            return false;
        }
    }
}
