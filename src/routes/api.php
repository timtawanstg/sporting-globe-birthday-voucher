<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('horizon.auth')->get('/commands', function (Request $request) {
    Artisan::command('horizon:purge', function ($project) {
        $this->info("Building {$project}!");
    })->describe('Terminate any rogue Horizon processes');

    Artisan::command('horizon:terminate', function ($project) {
        $this->info("Building {$project}!");
    })->describe('Terminate the master supervisor so it can be restarted');
});

Route::apiResource('voucher-account', 'API\VoucherAccountController');

