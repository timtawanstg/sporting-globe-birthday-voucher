FROM webdevops/php-nginx:ubuntu-18.04

ENV PROVISION_CONTEXT "production"
ENV WEB_DOCUMENT_ROOT "/var/www/public"
ENV WEB_DOCUMENT_INDEX "index.php"
ENV CLI_SCRIPT "php /var/www/public/index.php"

# Deploy scripts/configurations
COPY etc/ /opt/docker/etc/
# Create the log file to be able to run tail
RUN touch /var/log/cron.log

# Give execution rights on the cron job and php
RUN ln -sf /opt/docker/etc/cron/crontab /etc/cron.d/docker-boilerplate \
    && chmod 0644 /opt/docker/etc/cron/crontab \
    && echo >> /opt/docker/etc/cron/crontab

# Give execution rights on the php
RUN ln -sf /opt/docker/etc/php/${PROVISION_CONTEXT}.ini /opt/docker/etc/php/php.ini

# Configure volume/workdir
WORKDIR /var/www/

# Copy all files
COPY ./src /var/www

# Install packages
RUN composer install --optimize-autoloader --no-dev
RUN cp .env.prod .env
RUN php artisan config:clear
RUN php artisan config:cache
RUN php artisan key:generate
# RUN php artisan horizon:purge
# RUN php artisan horizon:terminate
RUN chmod -R a+w storage/ bootstrap/cache
